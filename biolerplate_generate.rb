require 'erb'

const_variables = {
	max_boxes_per_layer: 40,
	max_layers_per_pallet: 10,
	max_layer_types: 6,
	max_string_length: 255,
	UNINITIALIZED_STRING: %q('uninitialized'),
	UNINITIALIZED_INTEGER: 9999,
	CDirPath: %Q('UD:\\\\usr\\\\'),
  LOG_FILE: %q('pd_db.log'),
  NEW_LINE: %q('\010')
}

const_variables[:max_boxes_all] =  const_variables[:max_boxes_per_layer] * const_variables[:max_layers_per_pallet]

records_variables = {
	Mission:{
		name: 'string',
		creator: 'string',
		product: 'string',
		code: 'string',
		company: 'string'
		},

	Setting:{
		frame_line_in_index: 'integer',
		frame_line_in_position_x: 'real',
		frame_line_in_position_y: 'real',
		frame_line_in_position_z: 'real',
		frame_line_in_position_r: 'real',


		frame_line_out_index: 'integer',
		frame_line_out_position_x: 'real',
		frame_line_out_position_y: 'real',
		frame_line_out_position_z: 'real',
		frame_line_out_position_r: 'real',

		box_length: 'integer',
		box_width: 'integer',
		box_height: 'integer',
		box_weight: 'integer',

		box_x_off: 'integer',
		box_y_off: 'integer',
		box_z_off: 'integer',
		orient: 'boolean',

		tool_index: 'integer',
		tool_position_x: 'real',
		tool_position_y: 'real',
		tool_position_z: 'real',
		tool_position_a: 'real',
		tool_position_r: 'real',
		tool_position_e: 'real',

		length_wise: 'boolean',
		cross_wise: 'boolean',
		box_per_pick: 'integer',
		box_orient: 'boolean',

		pallet_length: 'integer',
		pallet_width: 'integer',
		pallet_height: 'integer',
		pallet_weight: 'integer',
		mini_distance: 'integer',
		sleepsheet_height: 'integer',

		tare: 'integer',
		max_gross: 'integer',
		max_height: 'integer',
		overhang_len: 'integer',
		overhang_wid: 'integer',
		max_pack: 'integer',
		},

	Box:{
		x: 'integer',
		y: 'integer',
		rotate: 'integer',
		arrow: 'integer',
		arrowEnabled: 'boolean',

		layer_name: 'string',
		is_available: 'boolean'
	},

	Layer:{
		name: 'string',
		is_available: 'boolean'
	},


}

tables_variables = {
	layers: 		{type: 'Layer',			count: "max_layer_types"},
	boxes_in_layer: {type: 'Box', 	count: "max_boxes_all"},
	used_layers: 	{type: 'Layer', 	count: "max_layers_per_pallet"}
}

single_variables = {
	mission_data:{
		type: :Mission
	},
	setting_data:{
		type: :Setting
	},
	request_box: {
		type: :Box
	},
	temp_boxes_count: {
		type: :integer
	},
}

# for ajax
# http://192.168.56.2/get?var=gui_record_1.Number&prog=gui_example_test_2
source_data = File.read('pd_db.erb')
program_name = 'pd_db'

template_data = ERB.new(source_data,nil, '-')

File.write("pd_db.pdl", template_data.result)  

# source_data = File.read('pd_db.erb')
# program_name = 'pg_command'

# template_data = ERB.new(source_data,nil, '-')

# File.write("pg_command.pdl", template_data.result) 




# data_program_name = 'pd_command'
# template_control = ERB.new(source_data,nil, '-')
# File.write("pd_command2.pdl", template_control.result)


# file_ajax = File.open("ajax_call.coffee", 'w')

# tables_variables.each do |var, var_info|
# 	var_record_type = records_variables[var_info[:type].to_sym]
# 	p var_info[:type]
# 	if var_record_type.nil?

# 	else
# 		var_record_type.each_key do |field|
# 				file_ajax.puts "FOR i := 1 to high DO"
# 				file_ajax.puts "#{url}/set?var=#{var}.#{field}[i]&prog=#{program_name}"
# 				file_ajax.puts "ENDFOR"

# 		end
# 	end

# end

