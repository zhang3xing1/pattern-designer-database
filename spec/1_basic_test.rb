require_relative 'pd_spec.rb'

pd = PDSpec.new

# pd.set_request

# pd.get_request

# pd.routine_request

pd.routine_request(name: 'resetAllData')

pd.set_request(name: 'mission_data.name', type: 'str', value: 'pd2_test_1')

pd.routine_request(name: 'saveVarFile', params:['pd2_test_1'])

pd.set_request(name: 'mission_data.name', type: 'str', value: 'pd2_test_2')

pd.routine_request(name: 'saveVarFile', params:['pd2_test_2'])

pd.routine_request(name: 'loadVarFile', params:['pd2_test_1'])

pd.get_request(name: 'mission_data')

