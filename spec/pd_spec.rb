require 'net/http'

class PDSpec
	def initialize
		# @host = 'http://192.168.56.2/'
		@host = 'http://172.22.117.53/'
		# @host = 'http://192.168.1.103:4242/'
		@program_name = 'pd_db2'
	end

	def set_request(options)
		# var_name
		# var_value
		# var_type

		if options[:type] == 'str'
			options[:value] = "'#{options[:value]}'"
		end		
		@url = "#{@host}set?var=#{options[:name]}&prog=#{@program_name}&value=#{options[:value]}"
		Net::HTTP.get(URI(@url))
		puts self

	end

	def get_request(options)
		# name	

		@url = "#{@host}get?var=#{options[:name]}&prog=#{@program_name}"
		puts Net::HTTP.get(URI(@url))

		puts self
	end

	def routine_request(options)
		# http://172.22.117.53/run?routine=init&prog=pd_command
		# routine name
		# routine parameters
		params = options[:params]
		unless params.nil? or params.empty? 
			params.map!{|param| param.class == String ? "'#{param}'" : param}
			params = "(#{params.join(',')})"
		else
			params = ''
		end
		@url = "#{@host}run?routine=#{options[:name]}#{params}&prog=#{@program_name}"
		Net::HTTP.get(URI(@url))
		puts self
	end

	def to_s
		"#{@url}\n\n"
	end
end