require_relative 'pd_spec.rb'

pd = PDSpec.new

pd.routine_request(name: 'loadVarFile', params:['pd2_test_2'])

pd.set_request(name: 'mission_data.creator', value: 'xing', type: 'str')

pd.get_request(name: 'mission_data')

pd.set_request(name: 'setting_data.frame_line_in_index', value: '12')
pd.routine_request(name: 'getFrameIn')

pd.set_request(name: 'setting_data.frame_line_out_index', value: '13')
pd.routine_request(name: 'getFrameOut')

pd.set_request(name: 'setting_data.tool_index', value: '4')
pd.routine_request(name: 'getTool')


pd.routine_request(name: 'addNewLayer', params:['layer_a'])

pd.routine_request(name: 'addNewLayer', params:['layer_b'])

pd.set_request(name: 'request_box.x', value: 1)
pd.set_request(name: 'request_box.y', value: 12)
pd.set_request(name: 'request_box.arrow', value: 45)
pd.set_request(name: 'request_box.arrowEnabled', value: 'true')
pd.set_request(name: 'request_box.layer_name', value: 'layer_a', type: 'str')
pd.set_request(name: 'request_box.rotate', value: 270)

pd.routine_request(name: 'addNewBox')

pd.set_request(name: 'request_box.x', value: 2)
pd.set_request(name: 'request_box.y', value: 12)
pd.set_request(name: 'request_box.arrow', value: 45)
pd.set_request(name: 'request_box.arrowEnabled', value: 'true')
pd.set_request(name: 'request_box.layer_name', value: 'layer_b', type: 'str')
pd.set_request(name: 'request_box.rotate', value: 270)

pd.routine_request(name: 'addNewBox')


pd.set_request(name: 'request_box.x', value: 3)
pd.set_request(name: 'request_box.y', value: 12)
pd.set_request(name: 'request_box.arrow', value: 45)
pd.set_request(name: 'request_box.arrowEnabled', value: 'true')
pd.set_request(name: 'request_box.layer_name', value: 'layer_b', type: 'str')
pd.set_request(name: 'request_box.rotate', value: 270)

pd.routine_request(name: 'addNewBox')

pd.set_request(name: 'request_box.x', value: 4)
pd.set_request(name: 'request_box.y', value: 12)
pd.set_request(name: 'request_box.arrow', value: 45)
pd.set_request(name: 'request_box.arrowEnabled', value: 'true')
pd.set_request(name: 'request_box.layer_name', value: 'layer_b', type: 'str')
pd.set_request(name: 'request_box.rotate', value: 270)

pd.routine_request(name: 'addNewBox')

pd.set_request(name: 'request_box.x', value: 5)
pd.set_request(name: 'request_box.y', value: 12)
pd.set_request(name: 'request_box.arrow', value: 45)
pd.set_request(name: 'request_box.arrowEnabled', value: 'true')
pd.set_request(name: 'request_box.layer_name', value: 'layer_a', type: 'str')
pd.set_request(name: 'request_box.rotate', value: 270)

pd.routine_request(name: 'addNewBox')

pd.routine_request(name: 'addNewUsedLayer', params:['layer_a'])
pd.routine_request(name: 'addNewUsedLayer', params:['layer_b'])
pd.routine_request(name: 'addNewUsedLayer', params:['layer_b'])
pd.routine_request(name: 'addNewUsedLayer', params:['layer_a'])

pd.routine_request(name: 'countOfBoxes', params: ['layer_a'])
pd.routine_request(name: 'countOfBoxes', params: ['layer_b'])


pd.set_request(name: 'mission_data.name', value: 'pd2_test_3', type: 'str')

pd.routine_request(name: 'saveVarFile', params:['pd2_test_3'])

pd.routine_request(name: 'loadVarFile', params:['pd2_test_3'])

pd.routine_request(name: 'findBox', params:[1, 'layer_b'])

pd.get_request(name: 'request_box')

pd.routine_request(name: 'findBox', params:[2, 'layer_b'])

pd.get_request(name: 'request_box')

pd.routine_request(name: 'findBox', params:[5, ''])

pd.get_request(name: 'request_box')

pd.routine_request(name: 'countOfBoxes', params: [''])

pd.get_request(name: 'temp_boxes_count')

# pd.routine_request(name: 'loadVarFile', params:['pd2_test_2'])
