PROGRAM JSONParse NOHOLD, STACK=65534
-- test\json_parse\JSONParse

--IMPORT 

  CONST
    PARSE_FILE = 'test_array.json'

    MAX_ARRAY_SIZE = 100
    MAX_STRING_LENGTH = 255
    LARGE_STRING = 16
    MAX_VAR_TYPE_LENGTH = 10
    MAX_KEY_NAME_LENGTH = 20
    CHAR_LENGTH = 1

    CDirPath = 'UD:\\usr\\test\\json_parse'
    LOG_FILE = 'JSONParse.log'
    NEW_LINE  = '\010'
  
  TYPE
    Bucket = RECORD
      bucket_id: Integer
      bucket_type: String[MAX_VAR_TYPE_LENGTH]
      bucket_value: String[MAX_STRING_LENGTH]
      key_name:  String[MAX_KEY_NAME_LENGTH]
      parent_array_id: Integer
      parent_object_id: Integer
    ENDRECORD
    
    ArrayBucket = RECORD
      array_id : Integer
      bucket_id: Integer
    ENDRECORD

    ObjectBucket = RECORD
      object_id : Integer
      bucket_id : Integer 
    ENDRECORD
    
  VAR

  log_index : Integer NOSAVE
  str_log_index : String[MAX_STRING_LENGTH] NOSAVE

  -- Bucket to be saved
  bucket_id : Integer NOSAVE
  buckets : ARRAY [MAX_ARRAY_SIZE] OF Bucket NOSAVE
  -- Object and array id to be saved
  object_id : Integer NOSAVE
  array_id  : Integer NOSAVE

  -- key_name for next bucket
  next_key_name : String[MAX_STRING_LENGTH] NOSAVE
  next_value : String[MAX_STRING_LENGTH] NOSAVE

  -- JsonParser
  large_source : ARRAY [LARGE_STRING] OF String[MAX_STRING_LENGTH]
  source : String[MAX_STRING_LENGTH] NOSAVE
  source_index : Integer NOSAVE
  str_source_index : String[MAX_STRING_LENGTH] NOSAVE
  source_char:   String[CHAR_LENGTH] NOSAVE


--ROUTINES
  ROUTINE writeStringFromFile(file_name : string; inputString : string; writeFormat : String): String EXPORTED FROM JSONParse
  ROUTINE Value : String EXPORTED FROM JSONParse

  ROUTINE LogDebug(log : String)
  VAR 
    result : String[MAX_STRING_LENGTH]
    str_clock :String[MAX_STRING_LENGTH]
  BEGIN
    ENCODE(str_log_index,log_index)
    result := ''
    --result := STR_CAT(result, str_clock)
    result := STR_CAT(result, '[Debug]')
    result := STR_CAT(result, str_log_index)
    result := STR_CAT(result, ': ')
    result := STR_CAT(result, log)
    result := STR_CAT(result, NEW_LINE)

    --writeStringFromFile(LOG_FILE,result, 'WA')

    log_index := log_index + 1
  END LogDebug

  ROUTINE LogDev(log : String)
  VAR 
    result : String[MAX_STRING_LENGTH]
    str_clock :String[MAX_STRING_LENGTH]
  BEGIN
    ENCODE(str_log_index,log_index)
    --ENCODE(str_clock, CLOCK)
    result := ''
    --result := STR_CAT(result, str_clock)
    result := STR_CAT(result, '[Dev] ')
    result := STR_CAT(result, str_log_index)
    result := STR_CAT(result, ': ')
    result := STR_CAT(result, log)
    result := STR_CAT(result, NEW_LINE)

    writeStringFromFile(LOG_FILE,result, 'WA')

    log_index := log_index + 1
  END LogDev

  ROUTINE readStringFromFile(file_name : string): String
  VAR 
    result: String[MAX_STRING_LENGTH]
    file_lun_read : Integer
  BEGIN
    LogDebug('read string from file ...')
    file_lun_read := 1
    OPEN FILE file_lun_read(file_name, 'R')
    Read file_lun_read(result)
    CLOSE FILE file_lun_read  

    RETURN(result)
  END readStringFromFile

  ROUTINE writeStringFromFile(file_name : string; inputString : string; writeFormat : String): String
  VAR 
    result: String[MAX_STRING_LENGTH]
    file_lun_write : Integer
  BEGIN
    result := ' '
    file_lun_write := 1
    OPEN FILE file_lun_write(file_name, writeFormat)
    Write file_lun_write(inputString)
    CLOSE FILE file_lun_write  
    RETURN(result)
  END writeStringFromFile

  ROUTINE Next(expect_char : string) : String
  VAR 
    str_bool : String[MAX_STRING_LENGTH]
    str_element_in_large_source : String[MAX_STRING_LENGTH]
    index_element_in_large_source : Integer
  BEGIN
    if(expect_char <> '') and (source_char <> expect_char) then 
      LogDebug('Next not plus 1')
      ENCODE(str_source_index, source_index)
      LogDebug(str_source_index)
      LogDebug('source_char <> ')
      ENCODE(str_bool, source_char <> '')
      LogDebug(str_bool)
      LogDebug('source_char <> expect_char ')
      ENCODE(str_bool, source_char <> expect_char)
      LogDebug(str_bool)

      LogDev('source_char')
      LogDev(source_char)
      LogDev('expect_char')
      LogDev(expect_char)

      return('')
    endif

    if source_index >= STR_LEN (source) then
      LogDebug('Next not plus 1')
      ENCODE(str_source_index, source_index)
      LogDebug(source_char)
      LogDebug(str_source_index)

      source_char := ''
      return('')
    endif
    source_index := source_index + 1

    index_element_in_large_source := source_index

    LogDebug('Next plused 1')
    ENCODE(str_source_index, source_index)
    --LogDebug(str_source_index)
    --LogDebug(source_char)

    STR_XTRT(source, source_index, 1, source_char)
    return(source_char)

  END Next

  ROUTINE White
  VAR 
    
  BEGIN
    while (source_char <> '') and (source_char <= ' ') do
      Next('')
    endwhile
  END White

  ROUTINE Number
  VAR 
    result : Integer
    str_result : String[MAX_STRING_LENGTH]
  BEGIN
    str_result := ''
    result := 0
    while (source_char >= '0') and (source_char <= '9') do
      str_result := STR_CAT(str_result, source_char)
      Next('')
    endwhile

  END Number

  ROUTINE Array_: String
  VAR 
    value_type : String[MAX_STRING_LENGTH]
    new_bucket : Bucket
  BEGIN
    if source_char = '[' then
      Next('[')

      -- new array

      new_bucket.bucket_id := bucket_id
      new_bucket.key_name := next_key_name
      ENCODE(new_bucket.bucket_value, array_id)
      new_bucket.bucket_type := 'array'

      buckets[bucket_id] := new_bucket

      array_id := array_id + 1
      bucket_id := bucket_id + 1    

      -- array element should not hava a KEY
      next_key_name := ''  

      if source_char = ']' then
        Next(']')
        return('')
      endif

      while source_char <> '' do
        value_type := Value

        -- array element should not hava a KEY
        next_key_name := '' 

        if value_type = 'integer' then
          new_bucket.bucket_id := bucket_id
          new_bucket.key_name := next_key_name
          new_bucket.bucket_value := next_value
          new_bucket.bucket_type := 'integer'

          buckets[bucket_id] := new_bucket

          bucket_id := bucket_id + 1
        endif


        if value_type = 'string' then
          LogDev('array element:')
          LogDev(next_value)
          LogDev('-')
          new_bucket.bucket_id := bucket_id
          new_bucket.key_name := next_key_name
          new_bucket.bucket_value := next_value
          new_bucket.bucket_type := 'string'

          buckets[bucket_id] := new_bucket

          bucket_id := bucket_id + 1
        endif

        if source_char = ']' then
          Next(']')
          LogDev('Object_ ] down')
          LogDev(str_source_index)
          return('')
        endif
        Next(',')
      endwhile
    endif
    LogDebug('Error in array')
  END Array_  

  ROUTINE Integer_: String
  VAR 
    result : String[MAX_STRING_LENGTH]
  BEGIN
    result := ''
    LogDebug('Integer_ start ')
    while (source_char >= '0') and (source_char <= '9') do
      result := STR_CAT(result, source_char)
      Next('')
    endwhile
    if result = '' then
      LogDebug('Error in string')
      return('')
    endif
    return(result)
  END Integer_

  ROUTINE String_ : String
  VAR 
    result : String[MAX_STRING_LENGTH]
  BEGIN
    result := ''
    LogDebug('String_ start ')
    if source_char = '"' then
      while Next('') <> '' do
        LogDebug('String_ char ')
        LogDebug(source_char)
        if source_char = '"' then
          Next('')
          LogDebug('[string] done!')
          return(result)
        endif
        result := STR_CAT(result, source_char)
      endwhile
    endif
    LogDebug('Error in string')
    return('')
  END String_

  ROUTINE Object_
  VAR 
    Key : String[MAX_STRING_LENGTH]
    str_result : String[MAX_STRING_LENGTH]
    value_type : String[MAX_STRING_LENGTH]
    new_bucket : Bucket
  BEGIN
    LogDebug('Object_ start ')
    --LogDebug(source_char)

    if source_char = '{' then
      Next('{')

      new_bucket.bucket_id := bucket_id
      new_bucket.key_name := next_key_name
      ENCODE(new_bucket.bucket_value, object_id)
      new_bucket.bucket_type := 'object'

      buckets[bucket_id] := new_bucket

      object_id := object_id + 1
      bucket_id := bucket_id + 1

      if source_char = '}' then
        Next('}')
        LogDev('Object_ } upper')
        LogDev(str_source_index)
        return
      endif
      while source_char <> '' do
        Key := String_
        LogDev('[Key]: .....')
        LogDev(Key)  

        -- set value to next_key_name
        next_key_name := Key    

        Next(':')

        value_type := Value

        if value_type = 'integer' then
          new_bucket.bucket_id := bucket_id
          new_bucket.key_name := next_key_name
          new_bucket.bucket_value := next_value
          new_bucket.bucket_type := 'integer'

          buckets[bucket_id] := new_bucket

          bucket_id := bucket_id + 1
        endif


        if value_type = 'string' then
          new_bucket.bucket_id := bucket_id
          new_bucket.key_name := next_key_name
          new_bucket.bucket_value := next_value
          new_bucket.bucket_type := 'string'

          buckets[bucket_id] := new_bucket

          bucket_id := bucket_id + 1
        endif

        if source_char = '}' then
          Next('}')
          LogDev('Object_ } down')
          LogDev(str_source_index)
          return
        endif
        Next(',')
      endwhile
    endif
    LogDebug('Error in object')
  END Object_

  ROUTINE Value : String
  VAR 
    str_bucket_id : String[MAX_STRING_LENGTH]
    new_bucket : Bucket
  BEGIN
    LogDebug('value start... ')
    LogDebug(source_char)

    if (source_char >= '0') and (source_char <= '9') then 
      next_value := Integer_
      LogDev('Integer')
      return('integer')
    endif
    if source_char = '{' then
      Object_  

      LogDev('object')
      return('object')
    endif
    if source_char = '"' then

      next_value := String_
      LogDev('string')
      LogDev('[VALUE]:')
      LogDev(next_value)

      return('string')  
    endif

    if source_char = '[' then

      next_value := Array_
      LogDev('array')
      LogDev(next_value)

      return('array')  
    endif   


    return('Error in Value')
  END Value

  ROUTINE ParseJson(source : string)
  VAR 
  BEGIN
    -- empty source return
    if source = '' then
      return
    endif
    Value
    LogDebug('paserjson done!')
    --White(source)
    if source_char <> '' then
      LogDebug('Error in ParseJson')
      LogDebug(source_char)
      ENCODE(str_log_index, source_index)
      LogDebug(str_log_index)
    endif
  END ParseJson

  ROUTINE PrintBuckets
  VAR 
    index : Integer
    a_bucket: Bucket
    buckets_number : Integer
  BEGIN
    buckets_number := bucket_id - 1
    FOR index := 1 to buckets_number DO
      a_bucket := buckets[index]
      write lun_crt (a_bucket.bucket_type, ' ', a_bucket.bucket_id, ': ', a_bucket.key_name, ' | ', a_bucket.bucket_value, NL)
    ENDFOR
  END PrintBuckets

  ROUTINE Reset
  VAR 
    
  BEGIN      
    log_index := 0
    DIR_SET(CDirPath)
    writeStringFromFile(LOG_FILE, '-----------', 'W')
  END Reset

  ROUTINE Init
  VAR 
    
  BEGIN
      Reset

      source := readStringFromFile(PARSE_FILE)
      source_index := 1
      source_char := ''
      STR_XTRT(source, source_index, 1, source_char)

      bucket_id := 1
      object_id := 1
      array_id  := 1
      next_key_name := ''
      next_value := ''

  END Init


BEGIN
  Init

  LogDebug('start parse....')

  ParseJson(source)

  PrintBuckets

  LogDebug('parse done!')

  --SYS_CALL('ms','JSONParse')

  
END JSONParse